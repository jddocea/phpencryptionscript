#!/bin/bash

touch ../.envTest;
touch ../keyfile;

./vendor/bin/generate-defuse-key > ../keyfile;

apiKeyEncrypted=$(php ./encrypt.php);

rm ../.envTest;
echo ENCRYPTED_API_KEY=\"$apiKeyEncrypted\" >> ../.envTest;

echo "PATH_TO_ENCRYPTION_KEY=\"../keyfile\"" >> ../.envTest;


