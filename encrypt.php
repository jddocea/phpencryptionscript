<?php
  if(file_exists('./vendor/autoload.php')) {
    require_once('./vendor/autoload.php');
  }
  use Defuse\Crypto\Key;
  use Defuse\Crypto\Crypto;
  #echo "Enter item you want to encrypt:";
  $handle = fopen ("php://stdin","r");
  $line = "apiKeyGoesHere";
  #echo "Enter the path to the encryption key:";
  $pathToKey = "../keyfile";
  $keyContents = file_get_contents(trim($pathToKey));
  $key = Key::loadFromAsciiSafeString($keyContents);
  $secret = Crypto::encrypt(trim($line), $key);
  echo $secret."\n";
 ?>
